#include <string.h>
#include <fstream>
#include <dirent.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <arpa/inet.h>

#include "socket_helper.h"

using namespace std;

/**

    This method assist in creating IP sockets and binding them to an IP address and a TCP port.
    The 'device' field is the name of the interface in Linux.

    @author Kim
    @version 0.1

*/

int get_ip_socket(std::string device, u_short port_no)
{
    int ip_sock;
    struct sockaddr_in sock_addr;
    struct ifreq ifr;

    if((ip_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        /* probably a permissions error */
        perror("Could not open a socket. You might want to check your permissions.");
        return -1;
    }

    // set SO_REUSEADDR on a socket to true (1):
    int opt = 1;
    if( setsockopt(ip_sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) )
        perror("Could not set socket option");


    memset(&sock_addr, 0, sizeof(sock_addr));
    memset(&ifr, 0, sizeof(ifr));

    /* get interface index  */
    strncpy((char *)ifr.ifr_name, device.c_str(), IFNAMSIZ);
    if((ioctl(ip_sock, SIOCGIFINDEX, &ifr)) == -1)
    {
        perror("Device not found.");
        return -1;  /* device not found */
    }

    /* Bind our ip socket to this interface */
    sock_addr.sin_addr.s_addr = INADDR_ANY;
    sock_addr.sin_family = AF_INET;
    sock_addr.sin_port = htons(port_no);

    if((bind(ip_sock, (const sockaddr *) &sock_addr, sizeof(sock_addr))) == -1)
    {
        perror("Could not bind.");
        return -1;  /* bind error */
    }

    return ip_sock;
}

/**

    This method assist in creating RAW sockets and binding them to an interface.
    Thus it will only return entire Ethernet frames from the specified interface.
    The 'device' field is the name of the interface in Linux.
    For getting a full Ethernet frame, protocol should be "ETH_P_ALL".

    @author Kim & Anders
    @version 0.1

*/

int get_raw_socket(string device, int protocol)
{
    int rawsock;
    struct sockaddr_ll sll;
    struct ifreq ifr;

    if((rawsock = socket(PF_PACKET, SOCK_RAW, htons(protocol))) == -1)
    {
        /* probably a permissions error */
        perror("Could not open a socket. You might want to check your permissions.");
        return -1;
    }

    memset(&sll, 0, sizeof(sll));
    memset(&ifr, 0, sizeof(ifr));

    /* get interface index  */
    strncpy((char *)ifr.ifr_name, device.c_str(), IFNAMSIZ);
    if((ioctl(rawsock, SIOCGIFINDEX, &ifr)) == -1)
    {
        perror("Device not found.");
        return -1;  /* device not found */
    }

    /* Bind our raw socket to this interface */
    sll.sll_family = AF_PACKET;
    sll.sll_ifindex = ifr.ifr_ifindex;
    sll.sll_protocol = htons(protocol);

    if((bind(rawsock, (struct sockaddr *)&sll, sizeof(sll))) == -1)
    {
        perror("Could not bind.");
        return -1;  /* bind error */
    }

    return rawsock;
}



/**
* Getting name and MAC address for interfaces that is not loopback.
* NOTE: Cannot register interfaces with names of length below 3.
* @param interface_names should be an empty vector for containing the interface names
* @param interface_addresses should be an empty vector for containing the interface MAC addresses
* @return content to input of the two vector pointers given as argument.
*/
int get_interface_info(vector<string>* interface_names, vector<string>* interface_addresses)
{
    // Open directory and see which interfaces that is available and has a physical address
    DIR* interfaces_dir;
    struct dirent* dir_pointer;
    string path = "/sys/class/net/";
    string to_append = "/address";

    interfaces_dir = opendir(path.c_str());
    if(interfaces_dir == nullptr)
    {
        perror("Could not open folder");
        printf("folder: %s\n\n",path.c_str());
        return EXIT_FAILURE;
    }

    // Iterating over subdirectories in the directory which represents an interface
    //vector<string> interface_names;
    //vector<string> interface_addressses;

    while((dir_pointer = readdir(interfaces_dir)) != nullptr)
    {
        string parsed = string(dir_pointer->d_name);
        // To filter out the loopback interface and non-valid entries. Thus this implementation only supports interface names of 3 or more characters.
        if(parsed.size() > 2)
        {
            interface_names->push_back(parsed);

            // Reading interface address
            string appended_path = path;
            appended_path+=parsed;
            appended_path+=to_append;
            ifstream t(appended_path); // declare object and open file

            string str((std::istreambuf_iterator<char>(t)),std::istreambuf_iterator<char>()); // reading all the content
            interface_addresses->push_back(str);
        }
        else printf("Will not add interface from folder: %s\n",parsed.c_str());
    }
    if(interface_names->size() == 0)
    {
        printf("No interfaces was found.");
        return EXIT_NO_INTERFACES;
    }

    return EXIT_SUCCESS;
}