//
// Created by pointer on 3/1/17.
//

#ifndef VOICE_RECOGNITION_SOCKET_HELPER_H
#define VOICE_RECOGNITION_SOCKET_HELPER_H

#include <string>
#include <vector>

//class socket_helper
//{
//public:
int get_ip_socket(std::string device, u_short port_no);
int get_raw_socket(std::string device, int protocol);
int get_interface_info(std::vector<std::string>* interface_names, std::vector<std::string>* interface_addresses);


const int EXIT_NO_INTERFACES = 2;
//};


#endif //VOICE_RECOGNITION_SOCKET_HELPER_H
