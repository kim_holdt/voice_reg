#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "decision_to_fpga.h"

#define HW_REGS_BASE ( 0xff200000 )
#define HW_REGS_SPAN ( 0x00200000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )
#define LED_PIO_BASE 0x0
/*
int main(int argc, char* argv[])
{
   char *value;

   if(argc >= 2) { value = argv[1]; }
   else { printf("Incorrect input vector\n");
          return -1;
   }

  // Convert arg to int
  unsigned int arg_int = stoi((const char *) value);

  alter_memory(arg_int);

  return 0;
}*/

int alter_memory(int val)
{
    volatile unsigned int *h2p_lw_led_addr = NULL;
	void *virtual_base;
	int fd;

    // Open /dev/mem
	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
		printf( "ERROR: could not open \"/dev/mem\"...\n" );
		return( 1 );
	}
    
    // get virtual addr that maps to physical
	virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );	
	if( virtual_base == MAP_FAILED ) {
		printf( "ERROR: mmap() failed...\n" );
		close( fd );
		return(1);
	}
    
    // Get the address that maps to the LEDs
	h2p_lw_led_addr=(unsigned int *)(virtual_base + (( LED_PIO_BASE ) & ( HW_REGS_MASK ) ));
   if (val == 1){
	    // Add 1 to the PIO register
	    unsigned int k = 1;	
	    *h2p_lw_led_addr = k;
    }
   else if(val == 3){
	   // Add 0 to the PIO register
	   unsigned int i  = 4; 
	   *h2p_lw_led_addr = i;
    }
   else if (val == 2){
	  unsigned int j = 2;
	 *h2p_lw_led_addr = j;
}
   else if (val == 4){
          unsigned int h = 8;
         *h2p_lw_led_addr = h;
}
	if( munmap( virtual_base, HW_REGS_SPAN ) != 0 ) {
		printf( "ERROR: munmap() failed...\n" );
		close( fd );
		return( 1 );

	}
	close( fd );
	return 0;
}
