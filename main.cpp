#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "socket_helper.h"
#include "SoCfiles/decision_to_fpga.cpp"

// Settings for computer
#define PORT_NUMBER 9500
#define INTERFACE_NAME "wlp4s0"
#define TURN_FILE "../turn_right.txt"
#define GO_FILE "../move_forward.txt"
#define STOP_FILE "../stop_now.txt"

// Settings for FPGA
//#define PORT_NUMBER 443
//#define INTERFACE_NAME "eth0"
//#define TURN_FILE "./turn_right.txt"
//#define GO_FILE "./move_forward.txt"
//#define STOP_FILE "./stop_now.txt"


#define MAX_WAITING_CONNECTIONS 5
#define BUFFER_SIZE 1000000 // megabyte size buffer
#define DATA_END_STRING ",X"

// For correlation
#define STEP 1
#define WINDOWS 5
#define RESULT_BOUNDARY 10000000000

using namespace std;

struct ThreadArgs {
    string fileName;
    vector<short> *inputData;
    vector<short> *referenceData;
    long result;
};

long correlation(long b1, vector<short> *v1, long b2, vector<short> *v2 ){
    int t = 0;
    long output = 0;

    if(b1 >= b2){
        while(b1 < v1->size() && t < v2->size()){
            output += v1->at(b1)*v2->at(t);

            b1++;
            t++;
        }
    }
    else{ // b2 > b1
        while(t < v1->size() && b2 < v2->size()){
            output += v1->at(t) * v2->at(b2);
            b2++;
            t++;
        }
    }
    return output;
}

//void *referenceMatch(vector<short> *input_data, vector<short> *test_data, long *correlationResult){
void *referenceMatch(void *arg){
    ThreadArgs *args = (ThreadArgs *) arg;
    args->result = 0;

    int x = 1;

    long aBegin = args->inputData->size() - x;
    long bBegin = 0;

    //cout << "In " << args->fileName << " status: starting matching." << endl;
    // Correlation with start
    while (aBegin > 0){
        long temp_corr = correlation(aBegin, args->inputData, bBegin, args->referenceData);
        aBegin-=STEP;
        if(aBegin < 0) aBegin = 0;
        if (temp_corr > args->result){
            args->result = temp_corr;
        }
    }

    //cout << "In " << args->fileName << " status: halfway." << endl;

    while (bBegin < args->referenceData->size() - x){
        //cout << "In " << args->fileName << " bBegin: " << bBegin << " & aBegin: " << aBegin <<  " & inputData.size() " << args->inputData->size()  << endl;
        long temp_corr = correlation(aBegin, args->inputData, bBegin, args->referenceData);
        bBegin+=STEP;
        if(bBegin > args->referenceData->size()) bBegin = args->referenceData->size();
        if (temp_corr > args->result){
            args->result = temp_corr;
        }
    }

    //cout << "In " << args->fileName << " status: done." << endl;
}

long correlation(long b1, long m1, vector<short> *v1, long b2, vector<short> *v2 ) {
    int t = 0;
    long output = 0;

    if (b1>=b2) {
        while (b1<m1 && t<v2->size()) {
            output += v1->at(b1)*v2->at(t);
            b1++;
            t++;
        }
    }
    else { // b2 > b1
        while (t<v1->size() && b2<v2->size()) {
            output += v1->at(t)*v2->at(b2);
            b2++;
            t++;
        }
    }

    return output;
}

//void *correlationMatch(vector<short> *input_data, vector<short> *test_data, long *correlationResult){
void *correlationMatch(void *arg){
    ThreadArgs *args = (ThreadArgs *) arg;

    // Defining size of windows that the input is split into
    long windowsSize, firstWindowSize;
    windowsSize = args->inputData->size() / WINDOWS;
    firstWindowSize = windowsSize + args->inputData->size() % WINDOWS; // is acceptable for small # of WINDOWS only!!!
    vector<double> results = vector<double>(WINDOWS, 0.0);

    for(unsigned int i = 0; i < WINDOWS; i++){
        long xLow = windowsSize * i;
        long xHigh = xLow + firstWindowSize;

        long bBegin = 0;

        cout << "In " << args->fileName << " status: round " << i << endl;

        while(bBegin + args->inputData->size() <= args->referenceData->size()){
            double temp_corr = correlation(xLow, xHigh, args->inputData, bBegin, args->referenceData);
            bBegin += STEP;
            if (temp_corr > results.at(i)) {
                results.at(i) = temp_corr;
            }
        }
    }

    cout << "In " << args->fileName << " status: done." << endl;

    // Take average and return via args.
    for(uint8_t i = 0; i < results.size(); i++) args->result += results.at(i);
    args->result /= (double) results.size();

    return arg;
}

// Method for converting string to vector<string>
const vector<string> explode(const string& s, const char& c) {
    string buff{""};
    vector<string> v;

    for(auto n:s)
    {
        if(n != c) buff+=n; else
        if(n == c && buff != "") { v.push_back(buff); buff = ""; }
    }
    if(buff != "") v.push_back(buff);

    return v;
}

int voice_reg(string input, vector<short> *test_data_turn, vector<short> *test_data_go, vector<short> *test_data_stop){
    vector<short> input_data;
    int returnCommand = 0;

    // Reading input data
    vector<string> v{explode(input, ',')};

    for(int i=0; i < v.size(); i++){
        input_data.push_back(stoi(v[i].c_str()));
    }

    ThreadArgs forTurn, forGo, forStop;

    forTurn.fileName = TURN_FILE;
    forGo.fileName = GO_FILE;
    forStop.fileName = STOP_FILE;


    forTurn.inputData = &input_data;
    forGo.inputData = &input_data;
    forStop.inputData = &input_data;


    forTurn.referenceData = test_data_turn;
    forGo.referenceData = test_data_go;
    forStop.referenceData = test_data_stop;


    forTurn.result = 0;
    forGo.result = 0;
    forStop.result = 0;


    pthread_t corrTurn, corrGo, corrStop, refTurn, refGo, refStop;

    //pthread_create(&corrTurn, nullptr, correlationMatch, (void *) &forTurn);
    //pthread_create(&corrGo, nullptr, correlationMatch, (void *) &forGo);
    //pthread_create(&corrStop, nullptr, correlationMatch, (void *) &forStop);
    //pthread_create(&refTurn, nullptr, referenceMatch, (void *) &forTurn);
    pthread_create(&refGo, nullptr, referenceMatch, (void *) &forGo);
    pthread_create(&refStop, nullptr, referenceMatch, (void *) &forStop);

    void *status;
    //pthread_join(corrTurn,&status);
    //pthread_join(corrGo,&status);
    //pthread_join(corrStop,&status);
    //pthread_join(refTurn,&status);
    pthread_join(refGo,&status);
    pthread_join(refStop,&status);

    cout << "Correlation values: " << endl;
    cout << "Turn: " << forTurn.result << endl;
    cout << "GO:  " << forGo.result << endl;
    cout << "Stop:  " << forStop.result << endl;

    cout << endl << "Deciding on: regular correlation " << endl;

    if(forStop.result > forGo.result)
    {
        returnCommand = 1;
        /*
        if(forTurn.result > forStop.result)
        {
            cout << "You said TURN "<< '\n';
            returnCommand = 3;
        }
        else
        {
            cout << "You said STOP "<< '\n';
            returnCommand = 1;
        }*/
    }
    else
    {
        returnCommand = 2;
        /*
        if(forGo.result > forStop.result)
        {
            cout << "You said GO " << '\n';
            returnCommand = 2;
        }
        else
        {
            cout << "You said STOP " << '\n';
            returnCommand = 1;
        }*/
    }

    return returnCommand;
}

int initialize(vector<short> *test_data_turn, vector<short> *test_data_go, vector<short> *test_data_stop){
    // Reads the saved data for START
    ifstream in_file_turn(TURN_FILE, ios::binary);
    //Check if the file is open
    if(!in_file_turn.is_open()){
        cout << "Reference file 'turn.txt' could not be opened..." << endl;
        return -1;
    }

    // Reads the saved data for STOP
    ifstream in_file_go(GO_FILE, ios::binary);
    //Check if the file is open
    if(!in_file_go.is_open()){
        cout << "Reference file 'go.txt' could not be opened..." << endl;
        return -1;
    }

    // Reads the saved data for STOP
    ifstream in_file_stop(STOP_FILE, ios::binary);
    //Check if the file is open
    if(!in_file_stop.is_open()){
        cout << "Reference file 'stop.txt' could not be opened..." << endl;
        return -1;
    }

    string data_array_turn, data_array_go, data_array_stop;

    in_file_turn >> data_array_turn;
    in_file_go >> data_array_go;
    in_file_stop >> data_array_stop;


    vector<string> v1{explode(data_array_turn, ',')};
    for(int i=0; i < v1.size(); i++){
        test_data_turn->push_back(stoi(v1[i].c_str()));
    }

    vector<string> v2{explode(data_array_go, ',')};
    for(int i=0; i < v2.size(); i++){
        test_data_go->push_back(stoi(v2[i].c_str()));
    }

    vector<string> v3{explode(data_array_stop, ',')};
    for(int i=0; i < v3.size(); i++){
        test_data_stop->push_back(stoi(v3[i].c_str()));
    }
}

int sendResponse(int message){
    int sock;
    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    socklen_t address_len = sizeof(sockaddr_in);
    sockaddr_in robotAddress;
    robotAddress.sin_addr = inet_makeaddr(171126784,239); // 10.51.48.239
    robotAddress.sin_port = htons(22205);
    robotAddress.sin_family = AF_INET;

    sockaddr_in fpgaAddress;
    //fpgaAddress.sin_addr = inet_makeaddr(171127040,107); // 10.51.49.107
    fpgaAddress.sin_addr = inet_makeaddr(168857344,180); // 10.16.143.180
    fpgaAddress.sin_port = htons(443);
    fpgaAddress.sin_family = AF_INET;

    bind(sock, (const sockaddr *) &fpgaAddress, address_len);
    connect(sock,(const sockaddr *) &robotAddress,address_len);

    string toSend = "";
    switch (message){
    case 1:
        toSend = "m0";
        break;
    case 2:
        toSend = "m255";
        break;
    default:
        break;
    }

    send(sock, toSend.c_str(), toSend.length(), 0);

    close(sock);

    return EXIT_SUCCESS;
}

int main() {
    vector<short> test_data_turn, test_data_go, test_data_stop;
    initialize(&test_data_turn, &test_data_go, &test_data_stop);

    cout << "Voice recognition server has started..." << endl;

    u_short port = PORT_NUMBER;
    string interface = INTERFACE_NAME;
    struct sockaddr_in serv_addr, cli_addr;
    char buffer[BUFFER_SIZE];

    get_ip_socket(interface,port);

    int newsockfd, sockfd = get_ip_socket(interface,port);

    if(sockfd <= 0)
        return  -1;

    // listen for file transfer
    listen(sockfd, MAX_WAITING_CONNECTIONS);

    socklen_t cli_len = sizeof(cli_addr);

    while(true) {
        newsockfd = accept(sockfd,
                           (struct sockaddr *) &cli_addr,
                           &cli_len);
        if (newsockfd < 0) {
            cout << "ERROR on accept" << endl;
        }

        long total = 0;
        string data = "";
        auto size = sizeof(DATA_END_STRING);
        string str = DATA_END_STRING;

        while (true) {
            // start thread for file transfer
            bzero(buffer, BUFFER_SIZE);
            auto n = read(newsockfd, buffer, BUFFER_SIZE);
            if (n < 0) {
                cout << "ERROR reading from socket" << endl;
                return -1;
            }

            printf("Here is the message: %s\n", buffer);
            data += string(buffer);
            cout << "With size: " << n << endl;
            total += n;

            int res = data.find('X', data.length() - 10);
            if (n == 0 || res > 0) break;


        }
        cout << "Total bytes rcvd: " << total << endl;

        // To remove unwanted termination characters.

        // For C++14
        //data.pop_back();
        //data.pop_back();

        // For C++11
        data.erase(data.length()-1);
        data.erase(data.length()-1);

        time_t before, after;
        time(&before);
        int res = voice_reg(data,&test_data_turn, &test_data_go, &test_data_stop);
        time(&after);

        double benchRes = difftime(after,before);
        cout << "Calculation took " << benchRes << " seconds." << endl;

        // Send response
        string sendBuf = to_string(res) + "\n";
        send(newsockfd, sendBuf.c_str(), sizeof(sendBuf), 0);
        cout << "Sending response string: " << sendBuf << endl;

        //Write response to FPGA LEDs
        //alter_memory(res);

        if(!sendResponse(res)) cout << "Command sent to robot with success" << endl;

        close(newsockfd);
    }

    close(sockfd);
    return 0;
}